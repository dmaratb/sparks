angular.module('imdbScanner', ['ui.bootstrap']).controller('ctrl', function ($scope, $http, $modal) {

    $scope.searchBaseUrl = 'http://www.omdbapi.com/?apikey=d777cf78&s=[searchterm]&type=movie&page=';
    $scope.detailsBaseUrl = 'http://www.omdbapi.com/?apikey=d777cf78&i=[imdbID]';

    $scope.init = function () {
        $scope.freeText = '';
        $scope.allMovies = [];
        $scope.pageNum = 1;
    }

    $scope.searchMovies = function () {

        if ($scope.freeText.length < 3) {
            alert('Atleast 3 characters');
            return;
        }

        $scope.pageNum = 1;
        $scope.allMovies.length = 0;
        $scope.searchUrl = $scope.searchBaseUrl.replace('[searchterm]', $scope.freeText);

        $scope.getResults();
    }

    $scope.loadMore = function () {
        if ($scope.allMovies.length < $scope.numOfResults) {
            $scope.pageNum++;
            $scope.getResults();
        }
    }

    /* API calls */
    $scope.getResults = function () {
        $http.get($scope.searchUrl + $scope.pageNum).then(function (response) {
            if (response.data.Error) {
                alert(response.data.Error);
            } else {
                $scope.allMovies = $scope.allMovies.concat(response.data.Search);
                $scope.numOfResults = response.data.totalResults;
            }
        }, function () {
            alert('connection failed');
        });
    }

    $scope.getMovieDetails = function (imdbID) {
        if ($scope.processing) {
            return;
        } else {
            $scope.processing = true;
        }

        var detailsUrl = $scope.detailsBaseUrl.replace('[imdbID]', imdbID);
        $http.get(detailsUrl).then(function (response) {
            $scope.processing = false;
            if (response.data.Error) {
                alert(response.data.Error);
            } else {
                $modal.open({
                    templateUrl: 'modalDetails.html',
                    controller: function ($scope, $modalInstance, movieDetails) {
                        $scope.details = movieDetails;
                        $scope.ok = function () { $modalInstance.close(); };
                        $scope.cancel = function () { $modalInstance.dismiss('cancel'); };
                    },
                    resolve: {
                        movieDetails: function () { return response.data; },
                    }
                });
            }
        }, function () {
            $scope.processing = false;
            alert('connection failed');
        });
    }

    $scope.init();
});
